Application SpringBoot Pokelab
============================

Nous avons réalisé un site web tournant autour de l’univers Pokémon.
Il n’est pas nécessaire de connaître le jeu pour comprendre le site web, la page d’accueil donne toutes les explications nécessaires.
Il est possible de consulter la liste des pokémons de la 1ère génération ou d’incuber soit même son Pokémon !
Une carte permet de répertorier tous les pokémons incubés de l’ensemble des utilisateurs.

L'application utilise Google Cloud Platform pour le déploiement et le datastore comme base de données.
Le site est disponible ici : https://flawless-parity-147007.appspot.com/index.html