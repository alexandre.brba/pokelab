package brba.poke;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

@RestController
public class QueueController {
    /*  Fonction REST qui permet a l utilisateur d'ajouter la creation d'un pokemon incubé dans la queue
	 * 
	 * @param incub
	 * 		Les données de l'incubation
	 * @return le status http de la demande
	 */
	@RequestMapping(value = "/addToQueue", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String AddToQueue(@RequestBody Incubation incub) throws Exception{
		  Queue queue = QueueFactory.getDefaultQueue();
		  queue.add(TaskOptions.Builder.withUrl("/incubate/"+incub.getType()+"/"+incub.getDresseur()+"/"+incub.getName()
		  +"/"+incub.getLatitude()+"/"+incub.getLongitude()).method(Method.PUT));
		  return "Requête ajoutée à la file";
	}
}
