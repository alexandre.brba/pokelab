package brba.poke;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;

@RestController
public class PokemonController {
	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	/* Fonction REST qui retourne un pokémon avec la clé (le nom) correspondant
	 * 
	 * @param key
	 * 		Le nom recherché
	 * @return Une instance de l'entité Pokemon, qui possède le nom recherché
	 */
    @RequestMapping(value="/pokemon/{key}", method=RequestMethod.GET) 
    @ResponseStatus(HttpStatus.OK) 
    public Entity getApokemon(@PathVariable("key") String key) throws EntityNotFoundException{
    	Key key2 = KeyFactory.createKey("Pokemon", key);
        Entity pokemon = datastore.get(key2);
        return pokemon;
    }
    
	/* Fonction REST qui supprime le pokémon correspondant
	 * 
	 * @param key
	 * 		Le nom du pokemon a supprimer
	 * @return le status http de la demande
	 */
    @RequestMapping(value="/pokemon/{key}", method=RequestMethod.DELETE) 
    @ResponseStatus(HttpStatus.OK)
    public void deleteApokemon(@PathVariable("key") String key) throws EntityNotFoundException{
    	Key key2 = KeyFactory.createKey("Pokemon", key);
        Entity pokemon = datastore.get(key2);
        datastore.delete(pokemon.getKey());
    }

	/* Fonction REST qui retourne la liste de tous les pokémons en fonction de leur numéro de pokédex (id) 
	 * 
	 * @return La liste des pokemons
	 */
    @RequestMapping(value="/pokemons", method=RequestMethod.GET) 
    @ResponseStatus(HttpStatus.OK) 
    public List<Entity> getListOfPokemons(){
        Query q = new Query("Pokemon").addSort("id",SortDirection.ASCENDING);
        List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
        return results;
    }

	/* Fonction REST qui retourne la liste de tous les pokémons en fonction du type recherché
	 * 
	 * @param type
	 * 		Le type de pokemon
	 * 
	 * @return La liste des pokemons de ce type
	 */
    @RequestMapping(value="/pokemons/{type}", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Entity> getListOfPokemonsOfThisType(@PathVariable("type") String type){
        return getFromType("type_primary",type);
    }
    
    /* Récupère la liste des pokemons en fonction du filtre donné
	 * 
	 * @param filter
	 * 		Le champ à filter
	 * @param val
	 * 		La valeur du filtre
	 * @return La liste des pokemons de ce filtre
	 */
    private List<Entity> getFromType(String filter, String val){
        Filter propertyFilter = new FilterPredicate(filter, FilterOperator.EQUAL, val);
        Query q = new Query("Pokemon").setFilter(propertyFilter);
        return datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
    }
    
    /*  Fonction REST qui permet la création des pokemons de la génération 1
	 * 
	 * @return le status http de la demande
	 */
    @RequestMapping(value="/init", method=RequestMethod.PUT) 
    @ResponseStatus(HttpStatus.OK)
    public void init(){
    	try (InputStream fr = getClass().getResourceAsStream("/PokemonDataset.csv")){
    		BufferedReader br = new BufferedReader(new InputStreamReader(fr));
            // read line by line
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split(";");
                Entity pokemon = new Entity("Pokemon",parts[1]);
                pokemon.setProperty("id", Integer.parseInt(parts[0]));
                pokemon.setProperty("name", parts[1]);
                pokemon.setProperty("type", "wild");
                pokemon.setProperty("stamina", Integer.parseInt(parts[2]));
                pokemon.setProperty("atk", Integer.parseInt(parts[3]));
                pokemon.setProperty("def", Integer.parseInt(parts[4]));
                pokemon.setProperty("capture_rate", Double.parseDouble(parts[5]));
                pokemon.setProperty("flee_rate", Double.parseDouble(parts[6]));
                pokemon.setProperty("spawn_chance", Double.parseDouble(parts[7]));
                pokemon.setProperty("type_primary", parts[8]);
                pokemon.setProperty("type_secondary", parts[9]);
                pokemon.setProperty("CP", Integer.parseInt(parts[10]));
                pokemon.setProperty("image_url", parts[11]);
                datastore.put(pokemon);
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }
    
    /*  Fonction REST pour l'incubation d'un pokemon par un utilisateur
	 * 
	 * @param type
	 * 		le type du pokemon
	 * @param dresseur
	 * 		le nom du dresseur
	 * @param name
	 * 		le nom du pokemon
	 * @param latitude
	 * 		la latitude lors de la création de l'oeuf
	 * @param longitude
	 * 		la longitude lors de la création de l'oeuf
	 * @return le status http de la demande
	 */
    @RequestMapping(value="/incubate/{type}/{dresseur}/{name}/{latitude}/{longitude}", method=RequestMethod.PUT) 
    @ResponseStatus(HttpStatus.OK)
    public void incubate(@PathVariable("type") String t, @PathVariable("dresseur") String d,@PathVariable("name") String n,
    		@PathVariable("latitude") Double lat,@PathVariable("longitude") Double lon) {
    	
    	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	Date date = new Date();
    	String creationDate = dateFormat.format(date);
    	
    	String type = t;
    	String dresseur = d;
    	String nom = n;
    	String type_secondary = "";
    	String url = "https://www.pokebip.com/pages/jeuxvideo/pokemon_cinquieme_generation_5g/vrai_z/shadow_ombre_z.png";
    	Double latitude = lat;
    	Double longitude = lon;
    	
    	//Initilisation des caractéristiques
    	long stamina = 1;
    	long atk = 1;
    	long def = 1;
    	Double capture = 1.0;
    	Double flee = 0.05;
    	Double spawn = 0.0;
    	long CP = 1;

    	try {
			List<Entity> list = getFromType("type_primary",type);
			Entity entity = getRandomElement(list);
			stamina = (long) entity.getProperty("stamina");
			CP = (long) entity.getProperty("CP");
			atk = (long) entity.getProperty("atk");
			def = (long) entity.getProperty("def");
			capture = (Double) entity.getProperty("capture_rate");
			spawn = (Double) entity.getProperty("spawn_chance");
			flee = (Double) entity.getProperty("flee_rate");
			url = (String) entity.getProperty("image_url");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
        try {
        	   Thread.sleep(10) ;
               Entity pokemon = new Entity("Pokemon",nom);
               pokemon.setProperty("dresseurName", dresseur);
               pokemon.setProperty("name", nom);
               pokemon.setProperty("type", "incubate");
               pokemon.setProperty("latitude", latitude);
               pokemon.setProperty("longitude", longitude);
               pokemon.setProperty("stamina", stamina);
               pokemon.setProperty("atk", atk);
               pokemon.setProperty("def", def);
               pokemon.setProperty("capture_rate", capture);
               pokemon.setProperty("flee_rate", flee);
               pokemon.setProperty("spawn_chance", spawn);
               pokemon.setProperty("type_primary", type);
               pokemon.setProperty("type_secondary", type_secondary);
               pokemon.setProperty("CP", CP);
               pokemon.setProperty("image_url", url);
               pokemon.setProperty("creationDate", creationDate);
               String description = "Un pokémon de type " + type
               + " crée le " + creationDate + " par le dresseur " + dresseur
               + ". CP="+CP+", Stamina="+stamina+", Attaque="+atk+", Defense="+def+", Taux de capture="+capture+", Taux d'échappe="
               + flee + ", Taux d'apparition="+spawn+", Type primaire="+type+", Type secondaire="+type_secondary;
               pokemon.setProperty("description", description);
               datastore.put(pokemon);
               System.out.println(description);
    	}  catch (InterruptedException e) {
    	   System.out.println("Error : I can't sleep");
    	}
    }
    
    /* Permet de retourner une entity d'une liste de façon aléatoire
 	 * 
 	 * @param list
 	 * 		La liste de Pokemon
 	 * @return l'entité aléatoire
 	 */
    public Entity getRandomElement(List<Entity> list){ 
        Random rand = new Random(); 
        return list.get(rand.nextInt(list.size())); 
    } 
    
	/* Fonction REST qui retourne la liste de tous les pokémons sauvages
	 * 
	 * @return La liste des pokemons sauvages
	 */
    @RequestMapping(value="/wild", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Entity> getListOfWildPokemons(){
        return getFromType("type","wild");
    }
    
	/* Fonction REST qui retourne la liste de tous les pokémons incubés par un joueur
	 * 
	 * @return La liste des pokemons incubés
	 */
    @RequestMapping(value="/incubate", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Entity> getListOfIncubatePokemons(){
        return getFromType("type","incubate");
    }

}
