package brba.poke;

//Classe qui permet l'incubation d'un pokemon
public class Incubation {
	private String type;
	private String dresseur;
	private String name;
	private Double longitude;
	private Double latitude;
	 
	public Incubation(String type, String dresseur, String name, Double longitude, Double latitude) {
		super();
		this.type = type;
		this.dresseur = dresseur;
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDresseur() {
		return dresseur;
	}

	public void setDresseur(String dresseur) {
		this.dresseur = dresseur;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
}
