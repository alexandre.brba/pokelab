package brba.poke;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PokelabApplication {
	
	/* Fonction main qui démarre le serveur
	 * 
	 * @param args
	 * 		Le tableau des arguments à passer en paramètres
	 */
	public static void main(String[] args) {
		SpringApplication.run(PokelabApplication.class, args);
	}

}
